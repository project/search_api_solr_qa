<?php declare(strict_types = 1);

namespace Drupal\search_api_solr_qa\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a solr qa ai response block.
 *
 * @Block(
 *   id = "search_api_solr_qa",
 *   admin_label = @Translation("Solr QA AI response"),
 *   category = @Translation("Search AI"),
 * )
 */
final class SearchApiSolrQaBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#markup' => '<div class="search-api-solr-qa-wrapper"></div>',
      '#allowed_tags' => ['div'],
      '#attached' => [
        'library' => [
          'search_api_solr_qa/search_api_solr_qa',
          ],
        ],
    ];
  }

}
