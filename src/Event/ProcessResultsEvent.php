<?php

namespace Drupal\search_api_solr_qa\Event;

use Drupal\Component\EventDispatcher\Event;

class ProcessResultsEvent extends Event {

  protected array $results;
  protected string $query;

  public function __construct(array $results = [], string $query = '') {
    $this->results = $results;
    $this->query = $query;
  }

  public function getResults(): array {
    return $this->results;
  }

  /**
   * @return string
   */
  public function getQuery(): string {
    return $this->query;
  }
}