<?php declare(strict_types = 1);

namespace Drupal\search_api_solr_qa\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for search_api_solr_qa routes.
 */
final class SearchApiSolrQaController extends ControllerBase {
  /**
   * Builds the response.
   */
  public function __invoke(Request $request) {
    $parameters = json_decode($request->getContent());
    $query = $parameters->question;
    $contexts = $parameters->context;
    /** @var \Drupal\openai\OpenAIApi $openai_api */
    $openai_api = \Drupal::service('openai.api');

    $prompt = <<<EOL
1. You are a helpful assistant
2. Refuse to answer questions that are not in the context 
3. ALWAYS reply to the query with the information provided by the context.
4. Never use sarcasm.
5. Never fill up content with unknown information.
6. Use basic HTML structure starting from "p"
7. If you feel like there isnt much context to generate an awnser, just cite the context.

Query: {$query}

Context:
{$contexts}
EOL;

    $response = $openai_api->completions('gpt-3.5-turbo-instruct', $prompt, 0.2, 512);

    return new Response($response, 200);
  }
}