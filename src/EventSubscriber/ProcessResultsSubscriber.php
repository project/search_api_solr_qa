<?php declare(strict_types = 1);

namespace Drupal\search_api_solr_qa\EventSubscriber;

use Drupal\search_api_solr_qa\Event\ProcessResultsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @todo Add description for this subscriber.
 */
final class ProcessResultsSubscriber implements EventSubscriberInterface {

  /**
   * Responds after retrieving results from solr.
   */
  public function onResultsResponse(ProcessResultsEvent $event): void {
    // @todo Place your code here.
    $results = $event->getResults();
    $excerpts = [];
    /** @var \Drupal\search_api\Item\Item $result */
    foreach ($results as $result) {
      $excerpts[] = $result->getExcerpt();
    }
    /** @var \Drupal\openai\OpenAIApi $openai_api */
    $openai_api = \Drupal::service('openai.api');
    $contexts = implode('\n', $excerpts);
    $query = $event->getQuery();

    $prompt = <<<EOL
1. You are a helpful assistant
2. ALWAYS reply to the query with the information provided by the context.
3. Never use sarcasm.
4. Be nice.

Query: {$query}

Context:
{$contexts}
EOL;

    drupal_static('query_qa', $contexts);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      'search_api_solr_qa.process_results' => 'onResultsResponse',
    ];
  }

}
