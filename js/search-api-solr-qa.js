// Check this: https://github.com/alexander-schranz/efficient-json-streaming-with-symfony-doctrine/blob/main/public/script.js

(function (Drupal, drupalSettings, $, once) {
  'use strict';
  Drupal.behaviors.searchApiSolrQaSearchApiSolrQa = {
    attach: function(context, settings) {
      once('search-api-solr-qa-wrapper', '.search-api-solr-qa-wrapper', context).forEach((wrapper) => {
        var wrapperArea = $(wrapper);
        var searchText = $('#edit-search-api-fulltext', context).val();

        fetch('/api/search-api-solr-qa/completions', {
          method: "POST",
          credentials: 'same-origin',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'text/event-stream',
          },
          body: JSON.stringify({
            question: searchText,
            context: drupalSettings.search_api_solr_qa.contexts
          })
        }).then(response => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          const reader = response.body.getReader();

          function processStream() {
            reader.read().then(({ done, value }) => {
              if (done) {
                console.log('Stream completed');
                return;
              }
              const chunkText = new TextDecoder("utf-8").decode(value);
              wrapperArea.append(chunkText + '<br>');
              processStream();
            }).catch(err => {
              console.error('Stream reading failed:', err);
              wrapperArea.html('Something went wrong during stream processing: ' + err.message);
            });
          }

          processStream();
        }).catch(error => {
          console.error('Fetch error:', error);
          wrapperArea.html('Something went wrong!: ' + error.message);
        });
      });
    }
  };
})(Drupal, drupalSettings, jQuery, once);
